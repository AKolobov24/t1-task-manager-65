package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.SessionDto;

import java.util.ArrayList;
import java.util.List;

public final class TestSession {

    @NotNull
    public static SessionDto createSession(@NotNull final String userId) {
        @NotNull SessionDto session = new SessionDto();
        session.setUserId(userId);
        return session;
    }

    @NotNull
    public static List<SessionDto> createSessionList(@NotNull final String userId) {
        @NotNull List<SessionDto> sessionList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull SessionDto session = new SessionDto();
            session.setUserId(userId);
            sessionList.add(session);
        }
        return sessionList;
    }

}
