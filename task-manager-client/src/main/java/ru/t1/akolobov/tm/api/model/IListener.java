package ru.t1.akolobov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.event.ConsoleEvent;

public interface IListener {

    @NotNull
    String getEventName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    void handleEvent(@NotNull final ConsoleEvent event);

}
