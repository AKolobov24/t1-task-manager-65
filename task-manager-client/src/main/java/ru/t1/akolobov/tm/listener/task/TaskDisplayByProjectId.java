package ru.t1.akolobov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.dto.request.TaskGetByProjectIdRequest;
import ru.t1.akolobov.tm.dto.response.TaskGetByProjectIdResponse;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskDisplayByProjectId extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-display-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Find tasks by project Id and display task list.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskDisplayByProjectId.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DISPLAY TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(getToken());
        request.setProjectId(id);
        @NotNull final TaskGetByProjectIdResponse response = getTaskEndpoint().getByProjectId(request);
        @NotNull final List<TaskDto> taskList = response.getTaskList();
        renderTaskList(taskList);
    }

}
