package ru.t1.akolobov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.akolobov.tm.dto.response.TaskGetByIdResponse;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.util.TerminalUtil;

@Component
public final class TaskDisplayByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-display-by-id";

    @NotNull
    public static final String DESCRIPTION = "Find task by Id and display.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskDisplayByIdListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DISPLAY TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);
        @NotNull final TaskGetByIdResponse response = getTaskEndpoint().getTaskById(request);
        @Nullable final TaskDto task = response.getTask();
        if (task == null) throw new TaskNotFoundException();
        displayTask(task);
    }

}
