package ru.t1.akolobov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    Project updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

}
