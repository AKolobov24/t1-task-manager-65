package ru.t1.akolobov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.akolobov.tm.repository.dto.TaskDtoRepository;

import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private final TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private final ProjectDtoRepository projectRepository;

    @Override
    @Transactional
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<TaskDto> tasks = taskRepository.findAllByUserIdAndProjectId(userId, projectId);
        tasks.forEach(t -> taskRepository.deleteByUserIdAndId(userId, t.getId()));
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDto task = taskRepository.findByUserIdAndId(userId, taskId)
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(final @Nullable String userId, final @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final TaskDto task = taskRepository.findByUserIdAndId(userId, taskId)
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskRepository.save(task);
    }

}
