package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestTaskDto.createTask;
import static ru.t1.akolobov.tm.data.dto.TestTaskDto.createTaskList;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Category(UnitCategory.class)
public final class TaskDtoRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);
    @NotNull
    private final static UserDtoRepository userRepository = context.getBean(UserDtoRepository.class);

    @NotNull
    private final static TaskDtoRepository repository = context.getBean(TaskDtoRepository.class);

    @NotNull
    private final static ProjectDtoRepository projectRepository = context.getBean(ProjectDtoRepository.class);

    @BeforeClass
    public static void prepareConnection() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @After
    public void clearData() {
        repository.deleteByUserId(USER1.getId());
        repository.deleteByUserId(USER2.getId());
    }

    @Test
    public void save() {
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.save(task);
        Assert.assertEquals(task, repository.findAllByUserId(USER1_ID).get(0));
        Assert.assertEquals(1, repository.findAllByUserId(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull final List<TaskDto> TaskList = createTaskList(USER1_ID);
        TaskList.forEach(repository::save);
        long size = repository.count();
        repository.deleteByUserId(USER1_ID);
        Assert.assertEquals(
                size - TaskList.size(),
                repository.count()
        );
    }

    @Test
    public void existById() {
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.save(task);
        Assert.assertTrue(repository.existsByUserIdAndId(USER1_ID, task.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER2_ID, task.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<TaskDto> user1TaskList = createTaskList(USER1_ID);
        @NotNull final List<TaskDto> user2TaskList = createTaskList(USER2_ID);
        user1TaskList.forEach(repository::save);
        user2TaskList.forEach(repository::save);
        Assert.assertEquals(user1TaskList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2TaskList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findByUserIdAndId() {
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.save(task);
        @NotNull final String taskId = task.getId();
        Assert.assertEquals(task, repository.findByUserIdAndId(USER1_ID, taskId).orElse(null));
        Assert.assertNull(repository.findByUserIdAndId(USER2_ID, taskId).orElse(null));
    }

    @Test
    public void getSize() {
        @NotNull final List<TaskDto> taskList = createTaskList(USER1_ID);
        taskList.forEach(repository::save);
        Assert.assertEquals(taskList.size(), repository.countByUserId(USER1_ID));
        repository.save(createTask(USER1_ID));
        Assert.assertEquals((taskList.size() + 1), repository.countByUserId(USER1_ID));
    }

    @Test
    public void remove() {
        createTaskList(USER1_ID).forEach(repository::save);
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.save(task);
        Assert.assertEquals(task, repository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null));
        repository.delete(task);
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null));
    }

    @Test
    public void removeById() {
        createTaskList(USER1_ID).forEach(repository::save);
        @NotNull final TaskDto task = createTask(USER1_ID);
        repository.save(task);
        repository.deleteByUserIdAndId(USER1_ID, task.getId());
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final ProjectDto project = new ProjectDto("test-project-for-task");
        project.setUserId(USER1_ID);
        projectRepository.save(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<TaskDto> taskList = createTaskList(USER1_ID);
        taskList.forEach(t -> t.setProjectId(projectId));
        taskList.forEach(repository::save);
        repository.save(createTask(USER1_ID));
        Assert.assertEquals(taskList, repository.findAllByUserIdAndProjectId(USER1_ID, projectId));
        taskList.forEach(repository::delete);
        projectRepository.deleteByUserIdAndId(USER1_ID, projectId);
    }

}
