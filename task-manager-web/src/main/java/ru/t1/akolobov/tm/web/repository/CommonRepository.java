package ru.t1.akolobov.tm.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.akolobov.tm.web.model.AbstractModel;

@NoRepositoryBean
public interface CommonRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}
