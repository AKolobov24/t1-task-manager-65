package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.api.endpoint.TaskRestEndpoint;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.model.Task;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public final class TaskEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") @NotNull final String id) {
        return taskService.findById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Task save(@RequestBody @NotNull final Task task) {
        return taskService.save(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        taskService.deleteById(id);
    }

}
