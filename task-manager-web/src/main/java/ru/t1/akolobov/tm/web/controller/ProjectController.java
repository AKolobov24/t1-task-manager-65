package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.enumerated.Status;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;

@Controller
public final class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        @NotNull final Project project = new Project();
        projectRepository.save(project);
        return "redirect:/project/edit?id=" + project.getId();
    }

    @NotNull
    @PostMapping("/project/delete")
    public String delete(
            @RequestParam(name = "id") @NotNull final String id
    ) {
        projectRepository.deleteById(id);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit")
    public ModelAndView edit(
            @RequestParam(name = "id") @NotNull final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        @Nullable final Project project = projectRepository.findById(id).orElse(null);
        if (project == null) {
            modelAndView.setStatus(HttpStatus.valueOf(404));
            return modelAndView;
        }
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/project/edit")
    public String edit(
            @ModelAttribute("project") @NotNull final Project project
    ) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

}
