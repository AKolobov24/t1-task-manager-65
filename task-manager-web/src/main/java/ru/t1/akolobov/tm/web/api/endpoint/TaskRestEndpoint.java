package ru.t1.akolobov.tm.web.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.model.Task;

import java.util.Collection;

public interface TaskRestEndpoint {

    @NotNull
    Collection<Task> findAll();

    @Nullable
    Task findById(@NotNull final String id);

    @Nullable
    Task save(@NotNull final Task task);

    void deleteById(@NotNull final String id);

}
